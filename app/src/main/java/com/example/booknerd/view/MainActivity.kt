package com.example.booknerd.view

import androidx.appcompat.app.AppCompatActivity
import com.example.booknerd.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity(R.layout.activity_main)