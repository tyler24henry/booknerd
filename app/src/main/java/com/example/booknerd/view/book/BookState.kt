package com.example.booknerd.view.book

import com.example.booknerd.model.response.Book

data class BookState(
    val isLoading: Boolean = false,
    val books: List<Book> = emptyList()
)
