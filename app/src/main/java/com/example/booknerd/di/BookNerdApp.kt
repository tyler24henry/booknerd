package com.example.booknerd.di

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class BookNerdApp : Application()