package com.example.booknerd.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.booknerd.databinding.ItemBookBinding
import com.example.booknerd.model.entity.Book

class BookAdapter : RecyclerView.Adapter<BookAdapter.BookViewHolder>() {

    private var books = mutableListOf<Book>()

    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int
    ): BookViewHolder = BookViewHolder.getInstance(parent)

    override fun onBindViewHolder(holder: BookViewHolder, position: Int) {
        val book = books[position]
        holder.loadBook(book)
    }

    override fun getItemCount(): Int = books.size

    fun loadBooks(books: List<Book>) {
        this.books = books.toMutableList()
        notifyDataSetChanged()
    }

    class BookViewHolder(private val binding: ItemBookBinding) : RecyclerView.ViewHolder(binding.root) {
        fun loadBook(book: Book) = with(binding) {
            tvTitle.text = book.title
        }
        companion object {
            fun getInstance(parent: ViewGroup) = ItemBookBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { binding -> BookViewHolder(binding) }
        }
    }
}