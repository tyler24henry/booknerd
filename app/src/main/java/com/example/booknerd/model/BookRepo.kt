package com.example.booknerd.model

import android.content.Context
import com.example.booknerd.model.entity.Book
import com.example.booknerd.model.remote.BookService
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class BookRepo @Inject constructor(private val bookService: BookService, @ApplicationContext context: Context) {

    val bookDao = BookDatabase.getInstance(context).bookDao()

    suspend fun getBooks() = withContext(Dispatchers.IO) {
        val cachedBooks: List<Book> = bookDao.getBooks()
        return@withContext cachedBooks.ifEmpty {
            val books: List<Book> = bookService.getBooks().map { book -> Book(title = book.title) }
            bookDao.insert(books)
            return@ifEmpty books
        }
    }
}