package com.example.booknerd.model.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.booknerd.model.entity.Book

@Dao
interface BookDao {
    @Query("SELECT * FROM book")
    suspend fun getBooks(): List<Book>

    @Insert
    suspend fun insert(books: List<Book>)
}