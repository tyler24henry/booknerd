package com.example.booknerd.model.remote

import com.example.booknerd.model.response.Book
import retrofit2.http.GET
import retrofit2.http.Path

interface BookService {
    @GET("books/{number}")
    suspend fun getBooks(@Path("number") count: Int = 100): List<Book>
}



















