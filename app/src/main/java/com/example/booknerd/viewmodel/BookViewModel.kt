package com.example.booknerd.viewmodel

import androidx.lifecycle.*
import com.example.booknerd.model.BookRepo
import com.example.booknerd.model.entity.Book
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BookViewModel @Inject constructor(private val bookRepo: BookRepo) : ViewModel() {
    private var _books = MutableLiveData<List<Book>>()
    val books: LiveData<List<Book>> get() = _books

    init {
        viewModelScope.launch {
            _books.value = bookRepo.getBooks()
        }
    }

}